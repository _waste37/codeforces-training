/* -----------------------
 *     Before an Exam 
 * -----------------------
 *  Author : Tiuna Angelini
 *  Date: 11-10-2022
 */

#include <stdio.h>


void
solve (int d, int sum_time) 
{
    int max_t[d], hours[d];
    int count = 0, pos = 0;
    int max_sum = 0;
    
    for (int i = 0; i < d; i++) {
        scanf ("%d%d", hours + i, max_t + i);
        count += hours[i];
        max_sum += max_t[i];
    }

    if (sum_time < count
            || sum_time > max_sum) {
        puts ("NO");
        return;
    }
    
    sum_time -= count;

    while (sum_time > 0) {
        if (sum_time >= max_t[pos] - hours[pos]) {
            sum_time = sum_time - max_t[pos] + hours[pos];
            hours[pos] = max_t[pos];
        } else {
            hours[pos] += sum_time;
            sum_time = 0;
        }
        pos++;
    }
    

    puts ("YES");
    for (int i = 0; i < d; i++) {
        printf ("%d ", hours[i]);
    }
    puts ("");
}

int 
main (void) 
{
    int d, sum_time;
    scanf ("%d%d", &d, &sum_time);


    solve (d, sum_time);
    return 0;
}
