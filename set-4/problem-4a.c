/* -----------------------
 *       Watermelon
 * -----------------------
 *  Author : Tiuna Angelini
 *  Date: 5-10-2022
 */

#include <stdio.h>

void
solve (int w) 
{
    if (w <= 2) {
        printf ("NO");
        return 0;
    }

    printf ("%s", (w & 1) == 0 ? "YES" : "NO");
}

int 
main (void) 
{
    int w;
    scanf ("%d", &w);

    solve (w);
    return 0;
}
