/* -----------------------
 *       Magic Box
 * -----------------------
 *  Author : Tiuna Angelini
 *  Date: 11-10-2022
 *  Class: 1600
 */

#include <stdio.h>

void
solve (int v1[static 3], int v[static 3], int faces[static 6]) 
{
    int sum = 0;
    // do i see face zox?
    if (v[1] < 0) {
        sum += faces[0];
    } else if (v[1] > v1[1]) {
        sum += faces[1];
    }

    if (v[2] < 0) {
        sum += faces[2];
    } else if (v[2] > v1[2]) {
        sum += faces[3]; 
    }

    if (v[0] < 0) {
        sum += faces[4];
    } else if (v[0] > v1[0]) {
        sum += faces[5]; 
    }

    printf ("%d\n", sum);
}

int 
main (void) 
{
    int v1[3], v[3];
    int faces[6];
    scanf ("%d%d%d", v, v + 1, v + 2);
    scanf ("%d%d%d", v1, v1 + 1, v1 + 2); 

    scanf ("%d%d%d%d%d%d",
            faces + 0, faces + 1, faces + 2, 
            faces + 3, faces + 4, faces + 5);

    solve (v1, v, faces);
    return 0;
}
