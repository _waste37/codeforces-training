/* -----------------------
 *         Team
 * -----------------------
 *  Author : Tiuna Angelini
 *  Date: 5-10-2022
 *  Class: 800
 */

#include <stdio.h>

void 
solve (int n) 
{
    int a, b, c, count = 0;
     
    for (int i= 0; i < n; i++) {
        scanf ("%d%d%d", &a, &b, &c); 

        if (a + b + c > 1) {
            count ++;
        }
    }

    printf ("%d\n", count);
}

int 
main (void) 
{
    int n; 
    scanf ("%d", &n);

    solve (n);
    return 0;
}
