/* -------------------------
 *  To Add or Not to Add
 * -------------------------
 *  Author : Tiuna Angelini
 *  Date: 11-10-2022
 *  Class: 1600
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int 
intcmp (const void *i1, const void *i2) 
{
    int a = *(int*)i1, b = *(int*)i2;

    if (a < b)      return -1;
    else if (a > b) return 1;
    else            return 0; 
}

void 
solve (int n, int k, long long *a)
{
    qsort (a+1, n, sizeof a[0], intcmp);
    
    long long part_sum[n+1];
    
    for (int i = 1; i <= n; i++) {
        part_sum[i] = part_sum[i-1] + (long long) a[i];
    }
   
    long long occurrences = 1, number = a[1];
    int count = 0; 
    for (int i = 2; i <= n; i++) {
        while (a[i] * (i - count) 
                - (part_sum[i] - part_sum[count]) > k) {
            count ++;
        }

        if (i - count > occurrences) {
            occurrences = i - count;
            number = a[i];
        }
    } 
    
    printf ("%lld %lld\n", occurrences, number);
}

int 
main (void) 
{
    int n, k;
    scanf ("%d%d", &n, &k);
    
    long long a[n+1];
    
    for (int i = 1; i <= n; i++) {
        scanf ("%lld", &a[i]);
    }

    solve (n, k, a); 
    return 0;
}
