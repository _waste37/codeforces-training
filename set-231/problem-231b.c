/* -----------------------------
 *  Magic, Wizardry and Wonders
 * -----------------------------
 *  Author : Tiuna Angelini
 *  Date: 11-10-2022
 *  Class: 1500
 */

#include <stdio.h>

void
solve (int n, int d, int l)
{
    int odd_positions = n / 2;
    int even_positions = (n + 1) / 2;
    
    int upper_bound = even_positions * l - odd_positions * 1;
    int lower_bound = even_positions * 1 - odd_positions * l;
    
    if ((upper_bound < d) || (lower_bound > d)) {
        printf ("%d\n", -1); /* d is out of bounds */
        return 0;
    }

    int a[n];

    for (int i = 0; i < n/2; i++) {
        a[i*2]   = l;
        a[i*2+1] = 1;
    }
    if ((n & 1) == 1) a[n-1] = l;

    int a_value = upper_bound;
    int pos = 0;
    while (a_value > d) {
        if ((((pos & 1) == 0) && (a[pos] > 1)) 
                || (((pos & 1) == 1) && (a[pos] < l))) {
            a[pos] = ((pos & 1) == 0) ? a[pos] - 1 : a[pos] + 1; 
            a_value --;
        } else { pos++; }
    }

    for (int i = 0; i < n; i++) {
        printf ("%d ", a[i]);
    }

    printf ("\n");
}

int 
main (void) 
{
    int n, d, l;
    scanf ("%d%d%d", &n, &d, &l);

/*
 * The key observation is that 
 * d = sum_{i=0}^{n/2} a[i*2] - sum_{i=0}^{n/2} a[i*2+1] 
 *   = sum of even positioned numbers - that of odd positioned
 */
   solve (n, d, l);
   return 0;
}
