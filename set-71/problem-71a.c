/* -----------------------
 *   Way too long words
 * -----------------------
 *  Author : Tiuna Angelini
 *  Date: 5-10-2022
 */

#include <stdio.h>
#define MAX_LEN 101

void 
solve (int n) 
{
    for (int i = 0; i < n; i++) {
        int count = 0;
        char word[MAX_LEN] = {0};
        
        while ((word[count++] = getchar ()) != '\n');
         
        if (count - 1 <= 10) {
            printf ("%s\n", word);
        } else {
            printf ("%c%d%c\n", word[0], count - 3, word[count - 2]);
        } 
    }
}

int 
main (void) 
{
    int n; 
    scanf ("%d\n", &n);

    solve (n);
    return 0;
}
