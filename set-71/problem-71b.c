/* -----------------------
 *     Progress Bar 
 * -----------------------
 *  Author : Tiuna Angelini
 *  Date: 28-10-2022
 */

#include <stdio.h>

void 
solve (int n, int k, int t) 
{
    int tot = ((float)t / 100.0f) * n * k;
    for (int i = 1; i <= n; i++) {
        int val;
        if (tot > k) {
            tot = tot - k;
            val = k;
        } else {
            val = tot;
            if (tot != 0) {
                tot = 0;
            }
        }

        printf ("%d ", val);
    }
}

int 
main (void) 
{
    int n, k, t;
    scanf ("%d%d%d", &n, &k, &t);

    solve (n, k ,t);
    return 0;
}
