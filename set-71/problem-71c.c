/* -----------------------
 *  Round Table Knights 
 * -----------------------
 *  Author : Tiuna Angelini
 *  Date: 28-10-2022
 */
#include <stdlib.h>
#include <stdio.h>

void 
solve (int n, int table[static n]) 
{
    for (int divisor = 1; divisor * 3 <= n; divisor++) {
        div_t d = div(n, divisor);
        if (d.rem != 0) {
            continue;
        } else if (d.quot < 3) {
            break;
        }

        for (int i = 0; i < divisor; i++) {
            int counter = 0; 
            for (int j = i; j < n; j += divisor) {
                counter += table[j]; 
            }
            if (counter == d.quot) {
                puts ("YES");
                return;
            }
        }
    }
    puts ("NO");
}

int 
main (void) 
{
    int n;
    scanf ("%d", &n);
    int table[n];

    for (int i = 0;  i < n; i++) {
        scanf ("%d", &table[i]);
    }

    solve (n, table);
    return 0;
}
